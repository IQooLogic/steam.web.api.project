package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class GenreDTO {
    private int id;
    private String description;

    public GenreDTO(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
