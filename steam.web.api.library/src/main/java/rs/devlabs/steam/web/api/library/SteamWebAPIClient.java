package rs.devlabs.steam.web.api.library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import rs.devlabs.steam.web.api.library.dto.CategoryDTO;
import rs.devlabs.steam.web.api.library.dto.GenreDTO;
import rs.devlabs.steam.web.api.library.dto.MetacriticDTO;
import rs.devlabs.steam.web.api.library.dto.MovieDTO;
import rs.devlabs.steam.web.api.library.dto.PlatformsDTO;
import rs.devlabs.steam.web.api.library.dto.PriceOverview;
import rs.devlabs.steam.web.api.library.dto.RecommendationsDTO;
import rs.devlabs.steam.web.api.library.dto.ReleaseDateDTO;
import rs.devlabs.steam.web.api.library.dto.RequirementsDTO;
import rs.devlabs.steam.web.api.library.dto.ScreenshotDTO;
import rs.devlabs.steam.web.api.library.dto.SteamAppDetailsDTO;
import rs.devlabs.steam.web.api.library.dto.SteamAppListDTO;
import rs.devlabs.steam.web.api.library.dto.SupportInfoDTO;
import rs.devlabs.steam.web.api.library.dto.WebMDTO;
import rs.devlabs.steam.web.api.library.exceptions.SteamWebAPIException;
import rs.devlabs.steam.web.api.library.utils.SteamWebAPIFieldNames;

/**
 *
 * @author milos
 */
public class SteamWebAPIClient {

    private final String APPDETAILS_URL = "http://store.steampowered.com/api/appdetails?appids=";
    private final String APPLIST_URL = "http://api.steampowered.com/ISteamApps/GetAppList/v2/";
    private final ObjectMapper mapper = new ObjectMapper();

    // TODO : implement custom exceptions
    public SteamAppDetailsDTO appDetails(long id) {
        try {
            String sid = String.valueOf(id);
            URL obj = new URL(APPDETAILS_URL + sid);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                StringBuilder response;
                try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                    String inputLine;
                    response = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }

                con.disconnect();

                Map<String, Object> map = mapper.readValue(response.toString(),
                    new TypeReference<HashMap<String, Object>>() {
                    });

                Map<String, Object> mr = mapRecursion(map, new HashMap<String, Object>());

                return getSteamAppDetailsDTO(mr);
            } else {
                System.out.println("GET request did't work");
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ProtocolException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Map<String, Object> mapRecursion(Map<String, Object> map, Map<String, Object> newMap) {

        for (String key : map.keySet()) {
            Object val = map.get(key);
            if (val instanceof Map) {
                if (isSpecial(key)) {
                    newMap.put(key, val);
                } else {
                    mapRecursion((Map) val, newMap);
                }
            } else {
                newMap.put(key, val);
            }
        }
        return newMap;
    }

    private boolean isSpecial(String key) {
        return key.equals("pc_requirements") || key.equals("mac_requirements")
            || key.equals("linux_requirements") || key.equals("platforms")
            || key.equals("metacritic") || key.equals("recommendations")
            || key.equals("release_date") || key.equals("support_info")
            || key.equals("price_overview");
    }

    private SteamAppDetailsDTO getSteamAppDetailsDTO(Map<String, Object> map) {
        SteamAppDetailsDTO sad = new SteamAppDetailsDTO();
        sad.setSuccess((boolean) map.get(SteamWebAPIFieldNames.SUCCESS));
        if (sad.isSuccess()) {
            sad.setName((String) map.get(SteamWebAPIFieldNames.NAME));
            sad.setType((String) map.get(SteamWebAPIFieldNames.TYPE));
            Object sai = map.get(SteamWebAPIFieldNames.STEAM_APPID);
            if (sai instanceof Integer) {
                sad.setSteam_appid((int) map.get(SteamWebAPIFieldNames.STEAM_APPID));
            } else {
                sad.setSteam_appid(Integer.valueOf((String) map.get(SteamWebAPIFieldNames.STEAM_APPID)));
            }
            Object ra = map.get(SteamWebAPIFieldNames.REQUIRED_AGE);
            if (ra instanceof Integer) {
                sad.setRequired_age((int) map.get(SteamWebAPIFieldNames.REQUIRED_AGE));
            } else {
                sad.setRequired_age(Integer.valueOf((String) map.get(SteamWebAPIFieldNames.REQUIRED_AGE)));
            }

            sad.setIs_free((boolean) map.get(SteamWebAPIFieldNames.IS_FREE));
            sad.setDlc((List<Long>) map.get(SteamWebAPIFieldNames.DLC));
            sad.setDetailed_description((String) map.get(SteamWebAPIFieldNames.DETAILED_DESCRIPTION));
            sad.setAbout_the_game((String) map.get(SteamWebAPIFieldNames.ABOUT_THE_GAME));
            sad.setSupported_languages((String) map.get(SteamWebAPIFieldNames.SUPPORTED_LANGUAGES));
            sad.setHeader_image((String) map.get(SteamWebAPIFieldNames.HEADER_IMAGE));
            sad.setWebsite((String) map.get(SteamWebAPIFieldNames.WEBSITE));
            Object pr = map.get(SteamWebAPIFieldNames.PC_REQUIREMENTS);
            if (pr instanceof Map) {
                Map<String, String> req = (Map<String, String>) map.get(SteamWebAPIFieldNames.PC_REQUIREMENTS);
                sad.setPc_requirements(new RequirementsDTO(req.get(SteamWebAPIFieldNames.REQUIREMENT_MINIMUM), RequirementsDTO.OS.PC));
            }

            Object mr = map.get(SteamWebAPIFieldNames.MAC_REQUIREMENTS);
            if (mr instanceof Map) {
                Map<String, String> req = (Map<String, String>) map.get(SteamWebAPIFieldNames.PC_REQUIREMENTS);
                sad.setMac_requirements(new RequirementsDTO(req.get(SteamWebAPIFieldNames.REQUIREMENT_MINIMUM), RequirementsDTO.OS.MAC));
            }

            Object lr = map.get(SteamWebAPIFieldNames.LINUX_REQUIREMENTS);
            if (lr instanceof Map) {
                Map<String, String> req = (Map<String, String>) map.get(SteamWebAPIFieldNames.PC_REQUIREMENTS);
                sad.setLinux_requirements(new RequirementsDTO(req.get(SteamWebAPIFieldNames.REQUIREMENT_MINIMUM), RequirementsDTO.OS.LINUX));
            }
            sad.setDevelopers((List<String>) map.get(SteamWebAPIFieldNames.DEVELOPERS));
            sad.setPublishers((List<String>) map.get(SteamWebAPIFieldNames.PUBLISHERS));
            Map<String, Object> price_overview = (Map<String, Object>) map.get(SteamWebAPIFieldNames.PRICE_OVERVIEW);
            if(price_overview != null) {
                String currency = (String) price_overview.get("currency");
                int initial = (int) price_overview.get("initial");
                int finalPrice = (int) price_overview.get("final");
                int discount_percentage = (int) price_overview.get("discount_percent");
                sad.setPriceOverview(new PriceOverview(currency, initial, finalPrice, discount_percentage));
            }
            sad.setPackages((List<String>) map.get(SteamWebAPIFieldNames.PACKAGES));
            sad.setPackage_groups((List<Object>) map.get(SteamWebAPIFieldNames.PACKAGE_GROUPS));// FIXME : DO NOT USE

            Map<String, Boolean> platforms = (Map<String, Boolean>) map.get(SteamWebAPIFieldNames.PLATFORMS);
            if (platforms != null) {
                sad.setPlatforms(new PlatformsDTO(platforms.get(SteamWebAPIFieldNames.PLATFORMS_WINDOWS),
                    platforms.get(SteamWebAPIFieldNames.PLATFORMS_MAC), platforms.get(SteamWebAPIFieldNames.PLATFORMS_LINUX)));
            }

            Map<String, Object> metacritic = (Map<String, Object>) map.get(SteamWebAPIFieldNames.METACRITIC);
            if (metacritic != null) {
                sad.setMetacritic(new MetacriticDTO((int) metacritic.get(SteamWebAPIFieldNames.METACRITIC_SCORE),
                    (String) metacritic.get(SteamWebAPIFieldNames.METACRITIC_URL)));
            }

            List<Map<String, Object>> categories = (List<Map<String, Object>>) map.get(SteamWebAPIFieldNames.CATEGORIES);
            if(categories != null){
                List<CategoryDTO> cats = new ArrayList<>();
                for(Map<String, Object> cat : categories){
                    cats.add(new CategoryDTO((int)cat.get(SteamWebAPIFieldNames.CATEGORY_ID),
                        (String)cat.get(SteamWebAPIFieldNames.CATEGORY_DESCRIPTION)));
                }
                sad.setCategories(cats);
            }
            
            List<Map<String, Object>> genres = (List<Map<String, Object>>) map.get(SteamWebAPIFieldNames.GENRES);
            if(genres != null){
                List<GenreDTO> gnrs = new ArrayList<>();
                for(Map<String, Object> gnr : genres){
                    gnrs.add(new GenreDTO(Integer.valueOf((String)gnr.get(SteamWebAPIFieldNames.GENRE_ID)),
                        (String)gnr.get(SteamWebAPIFieldNames.GENRE_DESCRIPTION)));
                }
                sad.setGenres(gnrs);
            }
            
            List<Map<String, Object>> screenshots = (List<Map<String, Object>>) map.get(SteamWebAPIFieldNames.SCREENSHOTS);
            if(screenshots != null){
                List<ScreenshotDTO> scrs = new ArrayList<>();
                for(Map<String, Object> scr : screenshots){
                    scrs.add(new ScreenshotDTO((int)scr.get(SteamWebAPIFieldNames.SCREENSHOT_ID),
                        (String)scr.get(SteamWebAPIFieldNames.SCREENSHOT_PATH_THUMBNAIL), (String)scr.get(SteamWebAPIFieldNames.SCREENSHOT_PATH_FULL)));
                }
                sad.setScreenshots(scrs);
            }
            
            List<Map<String, Object>> movies = (List<Map<String, Object>>) map.get(SteamWebAPIFieldNames.MOVIES);
            if(movies != null){
                List<MovieDTO> movs = new ArrayList<>();
                for(Map<String, Object> mov : movies){
                    Map<String, String> webm = (Map<String, String>)mov.get(SteamWebAPIFieldNames.MOVIE_WEBM);
                    movs.add(new MovieDTO((int)mov.get(SteamWebAPIFieldNames.MOVIE_ID),
                        (String)mov.get(SteamWebAPIFieldNames.MOVIE_NAME), (String)mov.get(SteamWebAPIFieldNames.MOVIE_THUMBNAIL), new WebMDTO((String)webm.get(SteamWebAPIFieldNames.MOVIE_WEBM_480), (String)webm.get(SteamWebAPIFieldNames.MOVIE_WEBM_MAX)), (boolean)mov.get(SteamWebAPIFieldNames.MOVIE_HIGHLIGHT)));
                }
                sad.setMovies(movs);
            }
            
            Map<String, Integer> recommendations = (Map<String, Integer>) map.get(SteamWebAPIFieldNames.RECOMMENDATIONS);
            if(recommendations != null){
                sad.setRecommendations(new RecommendationsDTO((int)recommendations.get(SteamWebAPIFieldNames.RECOMMENDATIONS_TOTAL)));
            }
            
            Map<String, Object> releaseDate = (Map<String, Object>) map.get(SteamWebAPIFieldNames.RELEASE_DATE);
            if(releaseDate != null){
                sad.setRelease_date(new ReleaseDateDTO((boolean)releaseDate.get(SteamWebAPIFieldNames.RELEASE_DATE_COMMING_SOON),
                    (String)releaseDate.get(SteamWebAPIFieldNames.RELEASE_DATE_DATE)));
            }
            
            Map<String, Object> supportInfo = (Map<String, Object>) map.get(SteamWebAPIFieldNames.SUPPORT_INFO);
            if(supportInfo != null){
                sad.setSupport_info(new SupportInfoDTO((String)supportInfo.get(SteamWebAPIFieldNames.SUPPORT_INFO_URL),
                    (String)supportInfo.get(SteamWebAPIFieldNames.SUPPORT_INFO_EMAIL)));
            }
            
            sad.setBackground((String) map.get(SteamWebAPIFieldNames.BACKGROUND));
        }

        return sad;
    }

    public SteamAppList appList() throws SteamWebAPIException {
        try {
            URL obj = new URL(APPLIST_URL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                StringBuilder response;
                try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                    String inputLine;
                    response = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }

                con.disconnect();

                SteamAppListDTO appList = mapper.readValue(response.toString(), SteamAppListDTO.class);
                return SteamAppListDTO.convert(appList);
            } else {
                throw new SteamWebAPIException(SteamWebAPIException.Cause.HTTP_ERROR, new Exception("HTTP error"));
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new SteamWebAPIException(SteamWebAPIException.Cause.HTTP_ERROR, ex);
        } catch (ProtocolException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new SteamWebAPIException(SteamWebAPIException.Cause.HTTP_ERROR, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new SteamWebAPIException(SteamWebAPIException.Cause.HTTP_ERROR, ex);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new SteamWebAPIException(SteamWebAPIException.Cause.MAPPING, ex);
        } catch (IOException ex) {
            Logger.getLogger(SteamWebAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new SteamWebAPIException(SteamWebAPIException.Cause.HTTP_ERROR, ex);
        }
    }
}
