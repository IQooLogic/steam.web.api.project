package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class RequirementsDTO {
    private String minimum;
    private OS os;
    
    public enum OS {
        PC, MAC, LINUX
    }

    public RequirementsDTO(String minimum, OS os) {
        this.minimum = minimum;
        this.os = os;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    public OS getOs() {
        return os;
    }

    public void setOs(OS os) {
        this.os = os;
    }
}
