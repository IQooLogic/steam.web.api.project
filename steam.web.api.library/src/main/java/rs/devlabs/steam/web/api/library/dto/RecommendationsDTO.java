package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class RecommendationsDTO {
    private int total;

    public RecommendationsDTO(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
