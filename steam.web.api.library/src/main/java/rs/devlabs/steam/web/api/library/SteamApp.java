package rs.devlabs.steam.web.api.library;

import rs.devlabs.steam.web.api.library.dto.SteamAppDTO;

/**
 *
 * @author milos
 */
public class SteamApp {

    private long appId;
    private String name;

    public SteamApp() {}

    public SteamApp(SteamAppDTO dto) {
        this.appId = dto.getAppid();
        this.name = dto.getName();
    }

    public SteamApp(long appId, String name) {
        this.appId = appId;
        this.name = name;
    }

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SteamApp{" + "appId=" + appId + ", name=" + name + '}';
    }
}
