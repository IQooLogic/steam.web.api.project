package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class MovieDTO {
    private int id;
    private String name;
    private String thumbnail;
    private WebMDTO webm;
    private boolean highlight;

    public MovieDTO(int id, String name, String thumbnail, WebMDTO webm, boolean highlight) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
        this.webm = webm;
        this.highlight = highlight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public WebMDTO getWebm() {
        return webm;
    }

    public void setWebm(WebMDTO webm) {
        this.webm = webm;
    }

    public boolean isHighlight() {
        return highlight;
    }

    public void setHighlight(boolean highlight) {
        this.highlight = highlight;
    }
}
