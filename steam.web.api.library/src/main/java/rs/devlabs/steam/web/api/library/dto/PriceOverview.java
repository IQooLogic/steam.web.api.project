package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class PriceOverview {
    private String currency;
    private int initial;
    private int finalPrice;// final
    private int discount_percent;

    public PriceOverview(String currency, int initial, int finalPrice, int discount_percent) {
        this.currency = currency;
        this.initial = initial;
        this.finalPrice = finalPrice;
        this.discount_percent = discount_percent;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getInitial() {
        return initial;
    }

    public void setInitial(int initial) {
        this.initial = initial;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(int discount_percent) {
        this.discount_percent = discount_percent;
    }
}
