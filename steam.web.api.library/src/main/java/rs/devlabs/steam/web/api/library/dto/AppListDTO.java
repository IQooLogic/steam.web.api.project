package rs.devlabs.steam.web.api.library.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author milos
 */
public class AppListDTO {
    private List<SteamAppDTO> apps = new ArrayList<>();

    public List<SteamAppDTO> getApps() {
        return apps;
    }
}
