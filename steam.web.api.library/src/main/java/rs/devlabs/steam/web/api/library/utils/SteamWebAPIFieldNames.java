package rs.devlabs.steam.web.api.library.utils;

/**
 *
 * @author milos
 */
public class SteamWebAPIFieldNames {
    public static final String SUCCESS = "success";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String STEAM_APPID = "steam_appid";
    public static final String REQUIRED_AGE = "required_age";
    public static final String IS_FREE = "is_free";
    public static final String DLC = "dlc";
    public static final String DETAILED_DESCRIPTION = "detailed_description";
    public static final String ABOUT_THE_GAME = "about_the_game";
    public static final String SUPPORTED_LANGUAGES = "supported_languages";
    public static final String HEADER_IMAGE = "header_image";
    public static final String WEBSITE = "website";
    public static final String PC_REQUIREMENTS = "pc_requirements";
    public static final String MAC_REQUIREMENTS = "mac_requirements";
    public static final String LINUX_REQUIREMENTS = "linux_requirements";
    public static final String DEVELOPERS = "developers";
    public static final String PUBLISHERS = "publishers";
    public static final String PRICE_OVERVIEW = "price_overview";
    public static final String PACKAGES = "packages";
    public static final String PACKAGE_GROUPS = "package_groups";
    public static final String PLATFORMS = "platforms";
    public static final String METACRITIC = "metacritic";
    public static final String CATEGORIES = "categories";
    public static final String GENRES = "genres";
    public static final String SCREENSHOTS = "screenshots";
    public static final String MOVIES = "movies";
    public static final String RECOMMENDATIONS = "recommendations";
    public static final String RELEASE_DATE = "release_date";
    public static final String SUPPORT_INFO = "support_info";
    public static final String BACKGROUND = "background";
    public static final String REQUIREMENT_MINIMUM = "minimum";
    public static final String PLATFORMS_WINDOWS = "windows";
    public static final String PLATFORMS_MAC = "mac";
    public static final String PLATFORMS_LINUX = "linux";
    public static final String METACRITIC_SCORE = "score";
    public static final String METACRITIC_URL = "url";
    public static final String CATEGORY_ID = "id";
    public static final String CATEGORY_DESCRIPTION = "description";
    public static final String GENRE_ID = "id";
    public static final String GENRE_DESCRIPTION = "description";
    public static final String SCREENSHOT_ID = "id";
    public static final String SCREENSHOT_PATH_THUMBNAIL = "path_thumbnail";
    public static final String SCREENSHOT_PATH_FULL = "path_full";
    public static final String MOVIE_ID = "id";
    public static final String MOVIE_NAME = "name";
    public static final String MOVIE_THUMBNAIL = "thumbnail";
    public static final String MOVIE_WEBM = "webm";
    public static final String MOVIE_WEBM_480 = "480";
    public static final String MOVIE_WEBM_MAX = "max";
    public static final String MOVIE_HIGHLIGHT = "highlight";
    public static final String RECOMMENDATIONS_TOTAL = "total";
    public static final String RELEASE_DATE_COMMING_SOON = "coming_soon";
    public static final String RELEASE_DATE_DATE = "date";
    public static final String SUPPORT_INFO_URL = "url";
    public static final String SUPPORT_INFO_EMAIL = "email";
}
