package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class ReleaseDateDTO {
    private boolean coming_soon;
    private String date;

    public ReleaseDateDTO(boolean coming_soon, String date) {
        this.coming_soon = coming_soon;
        this.date = date;
    }

    public boolean isComing_soon() {
        return coming_soon;
    }

    public void setComing_soon(boolean coming_soon) {
        this.coming_soon = coming_soon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
