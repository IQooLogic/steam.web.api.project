package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class MetacriticDTO {
    private int score;
    private String url;

    public MetacriticDTO(int score, String url) {
        this.score = score;
        this.url = url;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
