package rs.devlabs.steam.web.api.library.dto;

import java.util.List;

/**
 *
 * @author milos
 */
public class SteamAppDetailsDTO {
    private boolean success;
    private String type;
    private String name;
    private int steam_appid;
    private int required_age;
    private boolean is_free;
    private List<Long> dlc;
    private String detailed_description;
    private String about_the_game;
    private String supported_languages;
    private String header_image;
    private String website;
    private RequirementsDTO pc_requirements;
    private RequirementsDTO mac_requirements;
    private RequirementsDTO linux_requirements;
    private List<String> developers;
    private List<String> publishers;
    private PriceOverview priceOverview;
    private List<String> packages;
    private List<Object> package_groups;//
    private PlatformsDTO platforms;
    private MetacriticDTO metacritic;
    private List<CategoryDTO> categories;
    private List<GenreDTO> genres;
    private List<ScreenshotDTO> screenshots;
    private List<MovieDTO> movies;
    private RecommendationsDTO recommendations;
    private ReleaseDateDTO release_date;
    private SupportInfoDTO support_info;
    private String background;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSteam_appid() {
        return steam_appid;
    }

    public void setSteam_appid(int steam_appid) {
        this.steam_appid = steam_appid;
    }

    public int getRequired_age() {
        return required_age;
    }

    public void setRequired_age(int required_age) {
        this.required_age = required_age;
    }

    public boolean isIs_free() {
        return is_free;
    }

    public void setIs_free(boolean is_free) {
        this.is_free = is_free;
    }

    public List<Long> getDlc() {
        return dlc;
    }

    public void setDlc(List<Long> dlc) {
        this.dlc = dlc;
    }

    public String getDetailed_description() {
        return detailed_description;
    }

    public void setDetailed_description(String detailed_description) {
        this.detailed_description = detailed_description;
    }

    public String getAbout_the_game() {
        return about_the_game;
    }

    public void setAbout_the_game(String about_the_game) {
        this.about_the_game = about_the_game;
    }

    public String getSupported_languages() {
        return supported_languages;
    }

    public void setSupported_languages(String supported_languages) {
        this.supported_languages = supported_languages;
    }

    public String getHeader_image() {
        return header_image;
    }

    public void setHeader_image(String header_image) {
        this.header_image = header_image;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public RequirementsDTO getPc_requirements() {
        return pc_requirements;
    }

    public void setPc_requirements(RequirementsDTO pc_requirements) {
        this.pc_requirements = pc_requirements;
    }

    public RequirementsDTO getMac_requirements() {
        return mac_requirements;
    }

    public void setMac_requirements(RequirementsDTO mac_requirements) {
        this.mac_requirements = mac_requirements;
    }

    public RequirementsDTO getLinux_requirements() {
        return linux_requirements;
    }

    public void setLinux_requirements(RequirementsDTO linux_requirements) {
        this.linux_requirements = linux_requirements;
    }

    public List<String> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<String> developers) {
        this.developers = developers;
    }

    public List<String> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<String> publishers) {
        this.publishers = publishers;
    }

    public PriceOverview getPriceOverview() {
        return priceOverview;
    }

    public void setPriceOverview(PriceOverview priceOverview) {
        this.priceOverview = priceOverview;
    }

    public List<String> getPackages() {
        return packages;
    }

    public void setPackages(List<String> packages) {
        this.packages = packages;
    }

    public List<Object> getPackage_groups() {
        return package_groups;
    }

    public void setPackage_groups(List<Object> package_groups) {
        this.package_groups = package_groups;
    }

    public PlatformsDTO getPlatforms() {
        return platforms;
    }

    public void setPlatforms(PlatformsDTO platforms) {
        this.platforms = platforms;
    }

    public MetacriticDTO getMetacritic() {
        return metacritic;
    }

    public void setMetacritic(MetacriticDTO metacritic) {
        this.metacritic = metacritic;
    }

    public List<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

    public List<GenreDTO> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreDTO> genres) {
        this.genres = genres;
    }

    public List<ScreenshotDTO> getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(List<ScreenshotDTO> screenshots) {
        this.screenshots = screenshots;
    }

    public List<MovieDTO> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieDTO> movies) {
        this.movies = movies;
    }

    public RecommendationsDTO getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(RecommendationsDTO recommendations) {
        this.recommendations = recommendations;
    }

    public ReleaseDateDTO getRelease_date() {
        return release_date;
    }

    public void setRelease_date(ReleaseDateDTO release_date) {
        this.release_date = release_date;
    }

    public SupportInfoDTO getSupport_info() {
        return support_info;
    }

    public void setSupport_info(SupportInfoDTO support_info) {
        this.support_info = support_info;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }
}
