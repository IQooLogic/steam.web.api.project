package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class WebMDTO {
    private String _480;
    private String max;

    public WebMDTO(String _480, String max) {
        this._480 = _480;
        this.max = max;
    }

    public String get480() {
        return _480;
    }

    public void set480(String _480) {
        this._480 = _480;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }
}
