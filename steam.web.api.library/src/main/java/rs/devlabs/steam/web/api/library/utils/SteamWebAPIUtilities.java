package rs.devlabs.steam.web.api.library.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author milos
 */
public class SteamWebAPIUtilities {

    public static Image getImageFromUrl(String path) throws MalformedURLException, IOException {
        URL url = new URL(path);
        BufferedImage image = ImageIO.read(url);
        return image;
    }
    
    public static ImageIcon getImageIconFromUrl(String path) throws IOException{
        return new ImageIcon(getImageFromUrl(path));
    }
}
