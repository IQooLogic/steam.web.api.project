package rs.devlabs.steam.web.api.library;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author milos
 */
public class SteamAppList {

    private List<SteamApp> appList = new ArrayList<>();

    public List<SteamApp> getAppList() {
        return appList;
    }

    public void setAppList(List<SteamApp> appList) {
        this.appList = appList;
    }

    @Override
    public String toString() {
        return "SteamAppList{" + "appList=" + appList + '}';
    }
}
