package rs.devlabs.steam.web.api.library.dto;

/**
 *
 * @author milos
 */
public class SteamAppDTO {
    private long appid;
    private String name;

    public long getAppid() {
        return appid;
    }

    public void setAppid(long appid) {
        this.appid = appid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
