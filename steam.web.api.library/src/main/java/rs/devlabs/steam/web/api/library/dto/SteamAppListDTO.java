package rs.devlabs.steam.web.api.library.dto;

import java.util.ArrayList;
import java.util.List;
import rs.devlabs.steam.web.api.library.SteamApp;
import rs.devlabs.steam.web.api.library.SteamAppList;

/**
 *
 * @author milos
 */
public class SteamAppListDTO {

    private AppListDTO applist;

    public AppListDTO getApplist() {
        return applist;
    }

    public void setApplist(AppListDTO applist) {
        this.applist = applist;
    }

    public static SteamAppList convert(SteamAppListDTO obj) {
        SteamAppList list = new SteamAppList();
        AppListDTO appList = obj.getApplist();
        List<SteamApp> apps = new ArrayList<>();
        for (SteamAppDTO o : appList.getApps()) {
            apps.add(new SteamApp(o));
        }
        list.setAppList(apps);
        return list;
    }
}
