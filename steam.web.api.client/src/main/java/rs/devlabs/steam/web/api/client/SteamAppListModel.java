package rs.devlabs.steam.web.api.client;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import rs.devlabs.steam.web.api.library.SteamApp;
import rs.devlabs.steam.web.api.library.SteamAppList;

/**
 *
 * @author milos
 */
public class SteamAppListModel extends DefaultListModel {

    private List<SteamApp> appList = new ArrayList<>();
    
    @Override
    public int getSize() {
        return appList != null ? appList.size() : 0;
    }

    @Override
    public Object getElementAt(int index) {
        SteamApp app = appList.get(index);
        return app.getName();
    }

    @Override
    public boolean isEmpty() {
        return this.appList.isEmpty();
    }

    public List<SteamApp> getAppList() {
        return appList;
    }

    public void setAppList(List<SteamApp> appList) {
        this.appList = appList;
        fireContentsChanged(this, 0, appList.size());
    }
    
    public void setAppList(SteamAppList list){
        setAppList(list.getAppList());
    }
    
    public long getAppId(int index){
        SteamApp app = appList.get(index);
        long id = -1;
        if(app != null){
            id = app.getAppId();
        }
        return id;
    }
}
