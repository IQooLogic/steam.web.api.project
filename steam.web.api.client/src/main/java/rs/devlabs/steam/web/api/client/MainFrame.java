package rs.devlabs.steam.web.api.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import rs.devlabs.steam.web.api.library.SteamApp;
import rs.devlabs.steam.web.api.library.SteamAppList;
import rs.devlabs.steam.web.api.library.SteamWebAPIClient;
import rs.devlabs.steam.web.api.library.dto.SteamAppDetailsDTO;
import rs.devlabs.steam.web.api.library.exceptions.SteamWebAPIException;
import rs.devlabs.steam.web.api.library.utils.SteamWebAPIUtilities;

/**
 *
 * @author milos
 */
public class MainFrame extends javax.swing.JFrame {

    private SteamWebAPIClient api = new SteamWebAPIClient();
    private SteamAppListModel model = new SteamAppListModel();

    public MainFrame() {
        initComponents();
        pbWorkInProgress.setVisible(false);
        tfFilter.getDocument().addDocumentListener(new DocumentListener() {

            private List<SteamApp> appList = new ArrayList<>();

            @Override
            public void insertUpdate(DocumentEvent e) {
                filter();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filter();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filter();
            }

            private void filter() {
                List<SteamApp> apps = new ArrayList<>();
                if (appList.isEmpty()) {
                    appList = model.getAppList();
                }
                if (!tfFilter.getText().isEmpty()) {
                    for (SteamApp app : appList) {
                        if (app.getName().contains(tfFilter.getText())) {
                            apps.add(app);
                        }
                    }
                } else {
                    apps = appList;
                }
                model.setAppList(apps);
                updateAppCountLabel(apps);
            }
        });

        lSteamApps.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        lSteamApps.setModel(model);
        lSteamApps.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    SwingWorker sw = new SwingWorker<SteamAppDetailsDTO, Void>() {

                        @Override
                        protected SteamAppDetailsDTO doInBackground() throws Exception {
                            pbWorkInProgress.setVisible(true);
                            return api.appDetails(model.getAppId(lSteamApps.getSelectedIndex()));
                        }

                        @Override
                        protected void done() {
                            try {
                                SteamAppDetailsDTO appDetails = get();
                                
                                if(appDetails.getPriceOverview() != null) {
                                    int price = appDetails.getPriceOverview().getFinalPrice();
                                    BigDecimal bdDevisor = new BigDecimal(100);
                                    BigDecimal bdPrice = new BigDecimal(price);
                                    bdPrice = bdPrice.divide(bdDevisor);
                                    System.out.println(bdPrice.toString());
                                }
                                
                                lblAppDetails.setText("");
                                String path = appDetails.getHeader_image();
                                ImageIcon img = null;
                                if (path != null) {
                                    img = SteamWebAPIUtilities.getImageIconFromUrl(path);
                                }
                                lblAppDetails.setIcon(img);
                            } catch (IOException ex) {
                                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ExecutionException ex) {
                                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                            } finally {
                                pbWorkInProgress.setVisible(false);
                            }
                        }
                    };
                    sw.execute();
                }
            }
        });

        SwingWorker sw = new SwingWorker<SteamAppList, Void>() {
            
            @Override
            protected SteamAppList doInBackground() throws Exception {
                pbWorkInProgress.setVisible(true);
                SteamAppList appList = null;
                try {
                    appList = api.appList();
                } catch (SteamWebAPIException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                return appList;
            }

            @Override
            protected void done() {
                try {
                    SteamAppList list = get();
                    if (list != null) {
                        List<SteamApp> apps = new ArrayList<>();
                        for (SteamApp app : list.getAppList()) {
                            if (!app.getName().contains("Trailer")
                                && !app.getName().contains("SDK")
                                && !app.getName().contains("Beta")
                                && !app.getName().contains("Demo")
                                && !app.getName().contains("Teaser")
                                && !app.getName().contains("Movie")
                                && !app.getName().contains("Server")
                                && !app.getName().contains("Tool")
                                && !app.getName().contains("TestApp")
                                && !app.getName().contains("Ubisoft Test App")
                                && !app.getName().contains("Client")
                                && !app.getName().contains("Soundtrack")
                                && !app.getName().contains("soundtrack")
                                && !app.getName().contains("not in use")) {
                                apps.add(app);
                            }
                        }
                        Collections.sort(apps, new Comparator<SteamApp>() {

                            @Override
                            public int compare(SteamApp o1, SteamApp o2) {
                                String name1 = o1.getName();
                                String name2 = o2.getName();
                                return name1.compareTo(name2);
                            }
                        });
                        model.setAppList(apps);
                        updateAppCountLabel(apps);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ExecutionException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    pbWorkInProgress.setVisible(false);
                }
            }
        };

        sw.execute();
    }

    private void updateAppCountLabel(List<SteamApp> apps){
        lblAppCount.setText(String.format("{%s}", apps.size()));
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tfFilter = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        lSteamApps = new javax.swing.JList();
        lblAppCount = new javax.swing.JLabel();
        bClearFilter = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblAppDetails = new javax.swing.JLabel();
        pbWorkInProgress = new javax.swing.JProgressBar();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Steam App List");

        jSplitPane1.setDividerLocation(300);
        jSplitPane1.setResizeWeight(0.5);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel1.setText("Filter:");

        lSteamApps.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lSteamApps.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(lSteamApps);

        lblAppCount.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblAppCount.setText("{0}");

        bClearFilter.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        bClearFilter.setText("X");
        bClearFilter.setToolTipText("Clear filter");
        bClearFilter.setMargin(new java.awt.Insets(2, 2, 2, 2));
        bClearFilter.setMaximumSize(new java.awt.Dimension(23, 23));
        bClearFilter.setMinimumSize(new java.awt.Dimension(23, 23));
        bClearFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bClearFilterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfFilter)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bClearFilter, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblAppCount)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAppCount)
                    .addComponent(bClearFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel3);

        lblAppDetails.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblAppDetails.setText("app details");
        lblAppDetails.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        pbWorkInProgress.setIndeterminate(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAppDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                    .addComponent(pbWorkInProgress, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAppDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pbWorkInProgress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jSplitPane1.setRightComponent(jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bClearFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bClearFilterActionPerformed
        tfFilter.setText("");
    }//GEN-LAST:event_bClearFilterActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class
                .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClearFilter;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JList lSteamApps;
    private javax.swing.JLabel lblAppCount;
    private javax.swing.JLabel lblAppDetails;
    private javax.swing.JProgressBar pbWorkInProgress;
    private javax.swing.JTextField tfFilter;
    // End of variables declaration//GEN-END:variables
}
